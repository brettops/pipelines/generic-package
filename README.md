# Generic Package Pipeline

[![pipeline status](https://gitlab.com/brettops/pipelines/generic-package/badges/main/pipeline.svg)](https://gitlab.com/brettops/pipelines/generic-package/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

Publish generic packages to the [GitLab Package Registry](https://docs.gitlab.com/ee/user/packages/generic_packages/).

## Usage

Include the pipeline in the `.gitlab-ci.yml` file and add a shell glob pattern
to select files to upload:

```yaml
include:
  - project: brettops/pipelines/generic-package
    file: include.yml

variables:
  GENERIC_PACKAGE_FILES: "*.tar.gz *.zip"
```

The upload job runs during the `deploy` stage, so any files to upload must
exist prior to this.
